import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {LoginPage} from "../pages/login/login";
import {EventPage} from "../pages/event/event";
import {EventplannerPage} from "../pages/eventplanner/eventplanner";
import {CreatemailinglistPage} from "../pages/createmailinglist/createmailinglist";
import {TodolistPage} from "../pages/todolist/todolist";
import {VerifyinvitePage} from "../pages/verifyinvite/verifyinvite";


@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;

    rootPage: any = LoginPage;

    pages: Array<{ title: string, component: any }>;

    constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
        this.initializeApp();

        // used for an example of ngFor and navigation
        this.pages = [
            {title: 'My Event', component: EventPage},
            {title: 'Event Planner', component: EventplannerPage},
            {title: 'Mailing List', component: CreatemailinglistPage},
            {title: 'To-Do List', component: TodolistPage},
            {title: 'Verify Invite', component: VerifyinvitePage}
        ];

    }

    initializeApp() {
        this.platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            this.statusBar.backgroundColorByHexString("#AE00BB");
            this.splashScreen.hide();
        });
    }

    openPage(page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    }
}

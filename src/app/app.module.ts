import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {MyApp} from './app.component';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {LoginPage} from "../pages/login/login";
import {EventPage} from "../pages/event/event";
import {NeweventPage} from "../pages/newevent/newevent";
import {EventplannerPage} from "../pages/eventplanner/eventplanner";
import {CreatemailinglistPage} from "../pages/createmailinglist/createmailinglist";
import {TodolistPage} from "../pages/todolist/todolist";
import {VerifyinvitePage} from "../pages/verifyinvite/verifyinvite";

@NgModule({
    declarations: [
        MyApp,
        LoginPage,
        EventPage,
        NeweventPage,
        EventplannerPage,
        CreatemailinglistPage,
        TodolistPage,
        VerifyinvitePage
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp),
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        LoginPage,
        EventPage,
        NeweventPage,
        EventplannerPage,
        CreatemailinglistPage,
        TodolistPage,
        VerifyinvitePage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: ErrorHandler, useClass: IonicErrorHandler}
    ]
})
export class AppModule {
}

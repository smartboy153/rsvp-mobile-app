import {Component} from '@angular/core';
import {MenuController, NavController, NavParams, Platform} from 'ionic-angular';
import {StatusBar} from "@ionic-native/status-bar";
import {NeweventPage} from "../newevent/newevent";

/**
 * Generated class for the EventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
    selector: 'page-event',
    templateUrl: 'event.html',
})
export class EventPage {

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public platform: Platform,
                public statusBar: StatusBar,
                public menu: MenuController) {
        this.menu.enable(true);
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad EventPage');
    }

    addnewevent() {
        this.navCtrl.push(NeweventPage);
    }


}
